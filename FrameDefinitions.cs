/*
 * A library used for communication over the ProbeNet Protocol
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace ProbeNet.Communication
{
    /// <summary>
    /// Frame definitions.
    /// </summary>
	public static class FrameDefinitions
	{
        /// <summary>
        /// The length of the start byte
        /// </summary>
		public const int START_BYTE_LENGTH = 1;
        /// <summary>
        /// The length of the payload end byte
        /// </summary>
		public const int PAYLOAD_END_BYTE_LENGTH = 1;
        /// <summary>
        /// The length of the checksum
        /// </summary>
		public const int CHECKSUM_LENGTH = 4;
        /// <summary>
        /// The length of the end byte
        /// </summary>
		public const int END_BYTE_LENGTH = 1;
        /// <summary>
        /// The length of the overhead data
        /// </summary>
		public const int OVERHEAD =
				START_BYTE_LENGTH +
				PAYLOAD_END_BYTE_LENGTH +
				CHECKSUM_LENGTH +
				END_BYTE_LENGTH;
		
        /// <summary>
        /// The start byte
        /// </summary>
		public const byte START_BYTE = 2;
        /// <summary>
        /// The end byte
        /// </summary>
		public const byte END_BYTE = 3;
        /// <summary>
        /// The payload start byte
        /// </summary>
		public const byte PAYLOAD_END_BYTE = 17;
	}
}

