/*
 * A library used for communication over the ProbeNet Protocol
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using ProbeNet.Messages;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
#endif
using ProbeNet.Enums;
using ProbeNet.Messages.Raw;
using ProbeNet.Enums.Converting;

namespace ProbeNet.Communication
{
    /// <summary>
    /// Message decoder.
    /// </summary>
    public static class MessageDecoder
    {
        /// <summary>
        /// Decode the specified data.
        /// </summary>
        /// <returns>The decoded message.</returns>
        /// <param name="data">Data.</param>
        public static Message Decode(string data)
        {
#if MICRO_FRAMEWORK
            Hashtable obj = (Hashtable)Json.NETMF.JsonSerializer.DeserializeString(data);
            Message message = null;
            if (obj == null || !obj.Contains("body"))
            {
                throw new ArgumentException("Json data does not contain 'body'object");
            }
            Hashtable body = (Hashtable)obj["body"];
            if (body == null || !body.Contains("type"))
            {
                throw new ArgumentException("Json data does not contain 'type' attribute in 'body' object");
            }
            MessageHeader header = new MessageHeader();
            if (obj.Contains("header"))
            {
                header = new MessageHeader((Hashtable)obj["header"]);
            }
            MessageType messageType = MessageTypeConverter.Deserialize((string)body["type"]);
#else
            Message message = JsonConvert.DeserializeObject<GenericMessage<MessageBody>>(data);
            MessageType messageType = message.Body.Type;
#endif
            switch (messageType) {
                case MessageType.DeviceDescriptionRequest:
#if MICRO_FRAMEWORK
                    message = new Message(header, new DeviceDescriptionRequest((Hashtable)body));
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<DeviceDescriptionRequest>>(data);
#endif
                    break;
                case MessageType.MeasurementDescriptionRequest:
#if MICRO_FRAMEWORK
                                message = new Message(header, new MeasurementDescriptionRequest((Hashtable)body));
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<MeasurementDescriptionRequest>>(data);
#endif
                    break;
                case MessageType.SequenceDescriptionRequest:
#if MICRO_FRAMEWORK
                    message = new Message(header, new SequenceDescriptionRequest((Hashtable)body));
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<SequenceDescriptionRequest>>(data);
#endif
                    break;
                case MessageType.DeviceDescription:
#if MICRO_FRAMEWORK
                    message = null; // should not be received by micro devices
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<DeviceDescription>>(data);
#endif
                    break;
                case MessageType.MeasurementDescription:
#if MICRO_FRAMEWORK
                    message = null; // should not be received by micro devices
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<MeasurementDescription>>(data);
#endif
                    break;
                case MessageType.SequenceDescription:
#if MICRO_FRAMEWORK
                    message = null; // should not be received by micro devices
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<SequenceDescription>>(data);
#endif
                    break;
                case MessageType.Sequence:
#if MICRO_FRAMEWORK
                    message = null;
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<Sequence>>(data);
#endif
                    break;
                case MessageType.SequenceSeriesMetadata:
#if MICRO_FRAMEWORK
                    message = null;
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<SequenceSeriesMetadata>>(data);
#endif
                    break;
                case MessageType.Error:
#if MICRO_FRAMEWORK
                    message = null;
#else
                    message = JsonConvert.DeserializeObject<GenericMessage<Error>>(data);
#endif
                    break;
                default:
#if MICRO_FRAMEWORK
                    string exception = "Unknown message type: " + messageType + " (" + MessageTypeConverter.Serialize(messageType) + ")";
#else
                    string exception = String.Format("Unknown message type: \"{0}\"", message.Body.Type);
#endif
                    throw new ArgumentException(exception);
            }
            return message;
        }
    }
}