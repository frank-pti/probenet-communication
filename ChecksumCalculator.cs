/*
 * A library used for communication over the ProbeNet Protocol
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Frankpti.Checksum;

namespace ProbeNet.Communication
{
    /// <summary>
    /// Checksum calculator for calculating the CCITT CRC 16 checksum.
    /// </summary>
	public static class ChecksumCalculator
	{
        /// <summary>
        /// Calculates the CCITT CRC 16 checksum.
        /// </summary>
        /// <returns>The checksum.</returns>
        /// <param name="data">Data.</param>
		public static int CalculateChecksum(byte[] data)
		{
			return CalculateChecksum(data, 0, data.Length);
		}
		
        /// <summary>
        /// Calculates the CCITT CRC 16 checksum.
        /// </summary>
        /// <returns>The checksum.</returns>
        /// <param name="data">Data.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="length">Length.</param>
		public static int CalculateChecksum(byte[] data, int offset, int length)
		{
			return CCITT_CRC16.Calculate(data, offset, length);
		}
	}
}

