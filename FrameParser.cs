/*
 * A library used for communication over the ProbeNet Protocol
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Text;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif

namespace ProbeNet.Communication
{
    /// <summary>
    /// Frame parser.
    /// </summary>
    public class FrameParser
    {
#if MICRO_FRAMEWORK
        private Queue buffer;
#else
        private Queue<byte> buffer;
#endif
        private FrameParserState state;
        private byte[] payload;
        private int checksumCountdown;

        /// <summary>
        /// Delegatve void for frame parsed event.
        /// </summary>
        public delegate void FrameParsedDelegate(byte[] data);
        /// <summary>
        /// Occurs when frame parsed.
        /// </summary>
        public event FrameParsedDelegate FrameParsed;

        /// <summary>
        /// Delegate void for frame parsed string event
        /// </summary>
        public delegate void FrameParsedStringDelegate(string data);
        /// <summary>
        /// Occurs when frame parsed string.
        /// </summary>
        public event FrameParsedStringDelegate FrameParsedString;

        /// <summary>
        /// Delegate void for frame error event
        /// </summary>
        public delegate void FrameErrorDelegate(string error);
        /// <summary>
        /// Occurs when frame error.
        /// </summary>
        public event FrameErrorDelegate FrameError;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Communication.FrameParser"/> class.
        /// </summary>
        public FrameParser()
        {
            Reset();
        }

        /// <summary>
        /// Reset this instance.
        /// </summary>
        public void Reset()
        {
            state = FrameParserState.WaitingForStartByte;
        }

        /// <summary>
        /// Parse the specified data.
        /// </summary>
        /// <param name="data">Data.</param>
        public void Parse(byte[] data)
        {
            Parse(data, 0, data.Length);
        }

        /// <summary>
        /// Parse the specified data, beginning at offset with the specified length.
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="length">Length.</param>
        public void Parse(byte[] data, int offset, int length)
        {
            for (int i = offset; i < offset + length; i++) {
                ParseByte(data[i]);
            }
        }

        private void ParseByte(byte data)
        {
            switch (state) {
                case FrameParserState.WaitingForStartByte:
                    state = ReadStartByte(data);
                    break;
                case FrameParserState.ReadingPayload:
                    state = ReadPayload(data);
                    break;
                case FrameParserState.ReadingPayloadEndByte:
                    state = ReadPayloadEndByte(data);
                    break;
                case FrameParserState.ReadingChecksum:
                    state = ReadChecksum(data);
                    break;
                case FrameParserState.ReadingEndByte:
                    state = ReadEndByte(data);
                    break;
            }
        }

        private FrameParserState ReadStartByte(byte data)
        {
            if (data != FrameDefinitions.START_BYTE) {
                return FrameParserState.WaitingForStartByte;
            }
#if MICRO_FRAMEWORK
            buffer = new Queue();
#else
            buffer = new Queue<byte>();
#endif
            return FrameParserState.ReadingPayload;
        }

        private FrameParserState ReadPayload(byte data)
        {
            if (data == FrameDefinitions.PAYLOAD_END_BYTE) {
#if MICRO_FRAMEWORK
                payload = new byte[buffer.Count];
                buffer.CopyTo(payload, 0);
#else
                payload = buffer.ToArray();
#endif
                return ReadPayloadEndByte(data);
            } else {
                buffer.Enqueue(data);
                return FrameParserState.ReadingPayload;
            }
        }

        private FrameParserState ReadPayloadEndByte(byte data)
        {
            if (data != FrameDefinitions.PAYLOAD_END_BYTE) {
                NotifyFrameError(
                    "Received chr({0}) instead of payload end byte chr({1})",
                    data, FrameDefinitions.PAYLOAD_END_BYTE);
                return ReadStartByte(data);
            }

            checksumCountdown = FrameDefinitions.CHECKSUM_LENGTH;
#if MICRO_FRAMEWORK
            buffer = new Queue();
#else
            buffer = new Queue<byte>();
#endif
            return FrameParserState.ReadingChecksum;
        }

        private bool IsHexadecimal(byte data)
        {
            if (data >= '0' && data <= '9') {
                return true;
            }
            if (data >= 'A' && data <= 'F') {
                return true;
            }
            return false;
        }

        private FrameParserState ReadChecksum(byte data)
        {
            if (!IsHexadecimal(data)) {
                NotifyFrameError(
                    "Received non-hexadecimal chr({0}) in checksum",
                    data);
                return ReadStartByte(data);
            }
            buffer.Enqueue(data);
            checksumCountdown--;
            if (checksumCountdown == 0) {
#if MICRO_FRAMEWORK
                byte[] array = new byte[buffer.Count];
                buffer.CopyTo(array, 0);
                string str = new String(Encoding.UTF8.GetChars(array));
                int receivedChecksum = Convert.ToInt32(str, 16);
#else
                int receivedChecksum = int.Parse (Encoding.UTF8.GetString (buffer.ToArray()), System.Globalization.NumberStyles.HexNumber);
#endif
                int calculatedChecksum = ChecksumCalculator.CalculateChecksum(payload);
                if (receivedChecksum == calculatedChecksum) {
                    return FrameParserState.ReadingEndByte;
                } else {
                    NotifyFrameError("Checksum mismatch: received {0:X04}, calculated {1:X04}", receivedChecksum, calculatedChecksum);
                    return FrameParserState.WaitingForStartByte;
                }
            } else {
                return FrameParserState.ReadingChecksum;
            }
        }

        private FrameParserState ReadEndByte(byte data)
        {
            if (data != FrameDefinitions.END_BYTE) {
                NotifyFrameError(
                    "Received chr({0}) instead of end byte chr({1})",
                    data, FrameDefinitions.END_BYTE);
                return ReadStartByte(data);
            }

            NotifyFrameParsed(payload);
            return FrameParserState.WaitingForStartByte;
        }

        private void NotifyFrameParsed(byte[] data)
        {
            if (FrameParsed != null) {
                FrameParsed(data);
            }
            if (FrameParsedString != null) {
#if MICRO_FRAMEWORK
                FrameParsedString(new String(Encoding.UTF8.GetChars(data)));
#else
                FrameParsedString(Encoding.UTF8.GetString (data));
#endif
            }
        }

        private void NotifyFrameError(string reason, params object[] arguments)
        {
            if (FrameError != null) {
#if MICRO_FRAMEWORK
                StringBuilder builder = new StringBuilder();
                builder.Append(reason);
                builder.Append(",");
                foreach (object arg in arguments) {
                    builder.Append(arg.ToString());
                    builder.Append(";");
                }
                FrameError(builder.ToString());
#else
                FrameError(String.Format(reason, arguments));
#endif
            }
        }
    }
}
