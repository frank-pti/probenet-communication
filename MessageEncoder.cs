/*
 * A library used for communication over the ProbeNet Protocol
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using ProbeNet.Messages;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
#endif

namespace ProbeNet.Communication
{
    /// <summary>
    /// Message encoder.
    /// </summary>
    public static class MessageEncoder
    {
        /// <summary>
        /// Encodes the message to the specified stream.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="outputStream">Output stream.</param>
        public static void EncodeToStream(Message message, Stream outputStream)
        {
#if MICRO_FRAMEWORK
            string json = message.ToString();
            StreamWriter writer = new StreamWriter(outputStream);
            writer.Write(json);
            writer.Flush();
#else
            StreamWriter streamWriter = new StreamWriter(outputStream);
            JsonWriter jsonWriter = new JsonTextWriter(streamWriter);
            jsonWriter.Formatting = Formatting.Indented;
            JsonSerializer serializer = new JsonSerializer();
            serializer.Serialize(jsonWriter, message);
#endif
        }

        /// <summary>
        /// Encodes the message to string.
        /// </summary>
        /// <returns>The result string.</returns>
        /// <param name="message">Message.</param>
        public static string EncodeToString(Message message)
        {
#if MICRO_FRAMEWORK
            return Json.NETMF.JsonSerializer.SerializeObject(message);            
#else
            return JsonConvert.SerializeObject(message, Formatting.Indented);
#endif
        }
    }
}
