/*
 * A library used for communication over the ProbeNet Protocol
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Text;

namespace ProbeNet.Communication
{
    /// <summary>
    /// Class for encoding a frame.
    /// </summary>
    public static class FrameEncoder
    {
        /// <summary>
        /// Encode the specified payload.
        /// </summary>
        /// <param name="payload">Payload.</param>
        public static byte[] Encode(byte[] payload)
        {
            return Encode(payload, 0, payload.Length);
        }

        /// <summary>
        /// Encode the specified payload.
        /// </summary>
        /// <param name="payload">Payload.</param>
        public static byte[] Encode(string payload)
        {
            return Encode(Encoding.UTF8.GetBytes(payload));
        }

        /// <summary>
        /// Encode the specified payload beginning at offset with the specified length.
        /// </summary>
        /// <param name="payload">Payload.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="length">Length.</param>
        public static byte[] Encode(byte[] payload, int offset, int length)
        {
            if (offset + length > payload.Length) {
                throw new ArgumentException("The offset and length must fit for the payload",
                    "payload");
            }
            byte[] frame = new byte[length + FrameDefinitions.OVERHEAD];

            int iter = 0;
            frame[iter] = FrameDefinitions.START_BYTE;
            iter += FrameDefinitions.START_BYTE_LENGTH;

            Array.Copy(payload, offset, frame, iter, length);
            iter += length;

            frame[iter] = FrameDefinitions.PAYLOAD_END_BYTE;
            iter += FrameDefinitions.PAYLOAD_END_BYTE_LENGTH;

            int checksum = ChecksumCalculator.CalculateChecksum(payload, offset, length);

#if MICRO_FRAMEWORK
            string checksumString = checksum.ToString("x4");
#else
            string format = "{0,4:X4}";
            string checksumString = String.Format(format, checksum);
#endif
            Array.Copy(Encoding.UTF8.GetBytes(checksumString), 0, frame, iter, FrameDefinitions.CHECKSUM_LENGTH);
            iter += FrameDefinitions.CHECKSUM_LENGTH;

            frame[iter] = FrameDefinitions.END_BYTE;
            iter += FrameDefinitions.END_BYTE_LENGTH;

            return frame;
        }
    }
}

