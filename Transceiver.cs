/*
 * A library used for communication over the ProbeNet Protocol
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using ProbeNet.Messages;

namespace ProbeNet.Communication
{
    /// <summary>
    /// ProbeNet Message Transceiver.
    /// </summary>
    public class Transceiver
    {
        /// <summary>
        /// Occurs when frame error.
        /// </summary>
        public event FrameParser.FrameErrorDelegate FrameError;

        /// <summary>
        /// Delegate method for the send data event.
        /// </summary>
        public delegate void SendDataDelegate(byte[] data, int offset, int length);
        /// <summary>
        /// Occurs when send data.
        /// </summary>
        public event SendDataDelegate SendData;

        /// <summary>
        /// Delegate method for the message received event.
        /// </summary>
        public delegate void MessageReceivedDelegate(Message message);
        /// <summary>
        /// Occurs when message received.
        /// </summary>
        public event MessageReceivedDelegate MessageReceived;

        private FrameParser frameParser;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Communication.Transceiver"/> class.
        /// </summary>
        public Transceiver()
        {
            frameParser = new FrameParser();
            frameParser.FrameParsedString += HandleFrameParserFrameParsedString;
            frameParser.FrameError += HandleFrameParserFrameError;
        }

        private void HandleFrameParserFrameError(string error)
        {
            if (FrameError != null) {
                FrameError(error);
            }
        }

        /// <summary>
        /// Reset this instance.
        /// </summary>
        public void Reset()
        {
            frameParser.Reset();
        }

        /// <summary>
        /// Send the specified message.
        /// </summary>
        /// <param name="message">Message.</param>
        public void Send(Message message)
        {
            if (SendData != null) {
                string m = MessageEncoder.EncodeToString(message);
                byte[] frame = FrameEncoder.Encode(m);
                SendData(frame, 0, frame.Length);
            }
        }

        private void HandleFrameParserFrameParsedString(string data)
        {
            Message decodedMessage = MessageDecoder.Decode(data);
            NotifyMessageReceived(decodedMessage);
        }

        /// <summary>
        /// Data received.
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="length">Length.</param>
        public void DataReceived(byte[] data, int offset, int length)
        {
            frameParser.Parse(data, offset, length);
        }

        private void NotifyMessageReceived(Message message)
        {
            if (MessageReceived != null) {
                MessageReceived(message);
            }
        }
    }
}

