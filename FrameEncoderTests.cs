using System;
using NUnit.Framework;
using ProbeNet.Communication;
using System.Text;

namespace probenetcommunicationunittests
{
    [TestFixture]
    public class FrameEncoderTests
    {
        [Test]
        public void TestEmptyFrame()
        {
            byte[] encoded = FrameEncoder.Encode(new byte[0]);
            Assert.AreEqual("FFFF", Encoding.ASCII.GetString(encoded));
        }

        [Test]
        public void TestOneByte()
        {
            byte[] encoded = FrameEncoder.Encode(new byte[] { (byte)'A' });
            Assert.AreEqual("AB915", Encoding.ASCII.GetString(encoded));
        }

    }
}
