using System;
using NUnit.Framework;
using ProbeNet.Communication;
using System.Text;

namespace probenetcommunicationunittests
{
    [TestFixture()]
    public class FrameParserTests
    {
        private FrameParser parser;

        [SetUp]
        public void SetUp()
        {
            parser = new FrameParser();
            parser.FrameError += delegate(string error) {
                throw new Exception(error);
            };
        }

        [Test]
        public void TestEmptyFrame()
        {
            string parsed = null;
            parser.FrameParsedString += delegate(string data) {
                parsed = data;
            };

            parser.Parse(Encoding.ASCII.GetBytes("FFFF"));
            Assert.AreEqual("", parsed);
        }

        [Test]
        public void TestOneByte()
        {
            string parsed = null;
            parser.FrameParsedString += delegate(string data) {
                parsed = data;
            };

            parser.Parse(Encoding.ASCII.GetBytes("AB915"));
            Assert.AreEqual("A", parsed);
        }
    }
}

