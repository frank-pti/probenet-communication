/*
 * A library used for communication over the ProbeNet Protocol
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Threading;
using ProbeNet.Messages;
using Logging;    
#if !MICRO_FRAMEWORK
using Internationalization;
#endif

namespace ProbeNet.Communication
{
    /// <summary>
    /// ProbeNet channel.
    /// </summary>
    public class ProbeNetChannel
    {
        /// <summary>
        /// Delegate void for message received event.
        /// </summary>
        public delegate void MessageReceivedDelegate(ProbeNetChannel channel, Message message);
        /// <summary>
        /// Occurs when message received.
        /// </summary>
        public event MessageReceivedDelegate MessageReceived;

        /// <summary>
        /// Delegate void for stream closed event.
        /// </summary>
        public delegate void StreamClosedDelegate(ProbeNetChannel channel);
        /// <summary>
        /// Occurs when stream closed.
        /// </summary>
        public event StreamClosedDelegate StreamClosed;

        private Stream stream;
        private Transceiver transceiver;

        private Thread receiveThread;

        private byte[] buffer;
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Communication.ProbeNetChannel"/> class.
        /// </summary>
        /// <param name="stream">Stream.</param>
        /// <param name="name">Name.</param>
        public ProbeNetChannel(Stream stream, string name)
        {
            if (!stream.CanRead || !stream.CanWrite) {
                throw new ArgumentException("The stream must be able to read and write.", "stream");
            }
            buffer = new byte[1024];
            this.stream = stream;
            this.name = name;
            transceiver = new Transceiver();
            transceiver.MessageReceived += NotifyMessageReceived;
            transceiver.SendData += HandleSendData;
            StartCommunication();
        }

        /// <summary>
        /// Send the specified message.
        /// </summary>
        /// <param name="message">Message.</param>
        public void Send(Message message)
        {
            transceiver.Send(message);
        }

        /// <summary>
        /// Send a message containing the specified body.
        /// </summary>
        /// <param name="body">Body.</param>
        public void Send(IMessageBody body)
        {
            Send(new Message(new MessageHeader(Constants.DefaultLanguage, DateTime.Now, null), body));
        }

        /// <summary>
        /// Reply a message containing the answerBody to the sender of the request
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="request">Request defining the sender.</param>
        /// <param name="answerBody">Answer message body.</param>
        public void Reply(
#if !MICRO_FRAMEWORK
            I18n i18n,
#endif
            Message request, IMessageBody answerBody) 
        {
            Message answer = new Message(new MessageHeader(
#if MICRO_FRAMEWORK
                "en",
#else
                i18n.Language,
#endif
                DateTime.Now, request.Header.Tag), answerBody);
            Send(answer);
        }

        private void HandleSendData(byte[] data, int offset, int length)
        {
            stream.Write(data, offset, length);
        }

        private void NotifyMessageReceived(Message message)
        {
            if (MessageReceived != null) {
                MessageReceived(this, message);
            }
        }

        private void StartCommunication()
        {
            receiveThread = new Thread(ReceiveInThread);
#if !MICRO_FRAMEWORK
            receiveThread.IsBackground = true;
            receiveThread.Name = String.Format("ProbeNet Channel Receiver Thread {0}", name);
#endif
            receiveThread.Start();
        }

        /// <summary>
        /// Stop receiving.
        /// </summary>
        public void Stop()
        {
            if (receiveThread != null && receiveThread.IsAlive) {
                receiveThread.Abort();
            }
        }

        private void ReceiveInThread()
        {
            try {
                while (stream.CanRead) {
                    int received = stream.Read(buffer, 0, buffer.Length);
                    if (received == 0) {
                        NotifyStreamClosed();
                        return;
                    }
                    transceiver.DataReceived(buffer, 0, received);
                }
            } catch (Exception e) {
                Logger.Log(e, "Stopping receive thread");
                NotifyStreamClosed();
            }
        }

        private void NotifyStreamClosed()
        {
            if (StreamClosed != null) {
                StreamClosed(this);
            }
        }
    }
}

